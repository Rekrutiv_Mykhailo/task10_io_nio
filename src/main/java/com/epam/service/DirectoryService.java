package com.epam.service;

import java.io.File;
import java.io.IOException;

public class DirectoryService {

    private String text;

    public DirectoryService() {
        this.text = "";
    }

    public void loadContentDirectory(String path) throws IOException {
        File file = new File(path);
        if (file.exists()) {
            printDirectory(file, "");
        } else {
            System.out.println("directory do not exists");
        }
    }

    private void printDirectory(File file, String str) {
        text += str + "Directory: " + file.getName() + "\n";
        str = str + "  ";
        File[] fileNames = file.listFiles();
        for (File f : fileNames) {
            if (f.isDirectory()) {
                printDirectory(f, str);
            } else {
                text += str + "File: " + f.getName() + "\n";
            }
        }
    }

    public String getDataAboutDirectory() {
        return text;
    }
}

