package com.epam.service;

public class CheckingCorrectMenuException extends Exception {

    public CheckingCorrectMenuException() {
    }

    public CheckingCorrectMenuException(String message) {
        super(message);
    }
}
