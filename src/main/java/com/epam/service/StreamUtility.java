package com.epam.service;

import com.epam.model.Worker;

import java.io.*;

public class StreamUtility {

    public Worker checkSerializationDeserialization(Worker worker) throws IOException, ClassNotFoundException {
        ObjectOutputStream out = new ObjectOutputStream(new BufferedOutputStream(
                new FileOutputStream("text.txt")));
        out.writeObject(worker);
        out.close();
        ObjectInputStream in = new ObjectInputStream(new BufferedInputStream(
                new FileInputStream("text.txt")));
        Worker worker1 = (Worker) in.readObject();
        in.close();
        return worker1;
    }

    public long calculateTimeReadingWithoutBuffer(String pathFile) throws IOException {

        long startTime = System.currentTimeMillis();
        InputStream inputstream =
                new FileInputStream(pathFile);
        return calculateTimeReading(startTime, inputstream);
    }

    public long calculateTimeReadingWithBuffer(String pathFile, int bufferSize) throws IOException {

        long startTime = System.currentTimeMillis();
        InputStream inputstream =
                new BufferedInputStream(new FileInputStream(pathFile), bufferSize);
        return calculateTimeReading(startTime, inputstream);
    }

    private long calculateTimeReading(long startTime, InputStream inputstream) throws IOException {
        long difference;
        int data = inputstream.read();
        while (data != -1) {
            data = inputstream.read();
        }
        inputstream.close();
        difference = System.currentTimeMillis() - startTime;
        return difference;
    }
}
