package com.epam.service;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;

public class CommentService {
    public static void main(String[] args) throws IOException {
        new CommentService().findComments();
    }

    public String findComments() throws IOException {
        String comment = "";
        BufferedReader in = new BufferedReader(
                new InputStreamReader(new FileInputStream("Test.java")));
        while (true) {
            String line = in.readLine();
            if (line == null) {
                break;
            }
            int index = line.indexOf("//");
            if (index != -1) {
                comment = comment + line.substring(index) + "\n";
            }
        }
        in.close();

        return comment;
    }

}

