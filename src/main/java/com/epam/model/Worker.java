package com.epam.model;

import java.io.Serializable;
import java.util.Objects;

public class Worker extends Person implements Serializable {

    private String position;

    public Worker(String position) {
        this.position = position;
    }

    public Worker(String name, String surname, int age, boolean parent, String position) {
            super(name, surname, age, parent);
        this.position = position;
    }

    public String getPosition() {
        return position;
    }

    public void setPosition(String position) {
        this.position = position;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        Worker worker = (Worker) o;
        return Objects.equals(position, worker.position);
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), position);
    }

    @Override
    public String
    toString() {
        String s = super.toString();
        return "Worker{" + s +
                "position='" + position + '\'' +
                '}';
    }
}
