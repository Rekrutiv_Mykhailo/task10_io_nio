package com.epam;

import com.epam.view.MainView;

import java.io.IOException;

public class Starter {
    public static void main(String[] args) throws IOException, ClassNotFoundException {
        new MainView();
    }
}
