package com.epam.view;

import java.io.IOException;

@FunctionalInterface
public interface Runner {
    void run() throws IOException, ClassNotFoundException;
}
