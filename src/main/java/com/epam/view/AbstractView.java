package com.epam.view;

import com.epam.service.CheckingCorrectMenuException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.InputMismatchException;
import java.util.Scanner;

public class AbstractView {

    private Logger logger = LogManager.getLogger(AbstractView.class);

    public void output(String mess) {
        logger.info(mess);
    }

    public int checkingInputInt() {
        int number;
        Scanner in = new Scanner(System.in);
        try {
            number = in.nextInt();
        } catch (InputMismatchException e) {
            logger.error("incorrect input");
            number = checkingInputInt();
        }
        return number;
    }

    private int checkingInputMenu() throws CheckingCorrectMenuException {
        output("please enter item");
        int number = checkingInputInt();
        if (number >= 0 && number <= 4) {
            return number;
        } else {
            throw new CheckingCorrectMenuException("Please enter from 0 to 5");
        }
    }


    public Integer inputMenu() {
        int numb;
        try {
            numb = checkingInputMenu();
        } catch (CheckingCorrectMenuException e) {
            logger.error(e);
            numb = inputMenu();
        }
        return numb;
    }

}
