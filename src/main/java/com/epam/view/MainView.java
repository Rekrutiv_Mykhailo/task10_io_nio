package com.epam.view;

import com.epam.controller.MainController;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class MainView extends AbstractView {
    private List<String> menu;
    private Map<Integer, Runner> methodsMenu;
    private MainController mainController;

    public MainView() throws IOException, ClassNotFoundException {
        mainController = new MainController();
        menu = new ArrayList<>();
        methodsMenu = new LinkedHashMap<>();
        menu.add("  1 - test serialization");
        menu.add("  2 - equal difference between reading with buffer and without .");
        menu.add("  3 - equal difference between reading with different buffer size .");
        menu.add("  4 - show package content.");
        menu.add("  0 - finish.");
        methodsMenu.put(1, this::checkSerialization);
        methodsMenu.put(2, this::showDifferenceReadingwithBufferAndWithout);
        methodsMenu.put(3, this::showDifferenceInTimeWithDifferentBufferSize);
        methodsMenu.put(4, this::showPackageContent);
        methodsMenu.put(0, this::finish);
        loadMenu();
    }

    private void loadMenu() throws IOException, ClassNotFoundException {
        printMenu(menu);
        methodsMenu.get(inputMenu()).run();
    }

    public void printMenu(List<String> menu) {
        menu.forEach(this::output);
    }

    public void checkSerialization() throws IOException, ClassNotFoundException {
        output(mainController.checkSerialization());
        loadMenu();
    }

    public void showDifferenceReadingwithBufferAndWithout() throws IOException, ClassNotFoundException {
        output(mainController.showDifferenceInTimeReadingWithBufferAndWithout());
        loadMenu();
    }

    public void showDifferenceInTimeWithDifferentBufferSize() throws IOException, ClassNotFoundException {
        output("please enter buffer size");
        output(mainController.showDifferenceInTimeReadingWithDifferentBufferSize(checkingInputInt()));
        loadMenu();
    }

    public void showPackageContent() throws IOException, ClassNotFoundException {
        output(mainController.showPackageContent());
        loadMenu();
    }

    public void finish() {

    }
}
