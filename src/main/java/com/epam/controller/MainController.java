package com.epam.controller;

import com.epam.model.Worker;
import com.epam.service.DirectoryService;
import com.epam.service.StreamUtility;

import java.io.IOException;

public class MainController {

    private String data;
    private StreamUtility streamUtility;
    private DirectoryService directoryService;

    public MainController() {
        data = "";
        streamUtility = new StreamUtility();
        directoryService = new DirectoryService();
    }

    public String checkSerialization() throws IOException, ClassNotFoundException {
        Worker worker = new Worker("Mykhailo", "Rekrutiv", 22, true, "programmer");
        data = worker.toString() + "\n";
        data += streamUtility.checkSerializationDeserialization(worker).toString();
        return data;
    }

    public String showPackageContent() throws IOException {
        directoryService.loadContentDirectory("C:\\Activators");
        return directoryService.getDataAboutDirectory();
    }

    public String showDifferenceInTimeReadingWithBufferAndWithout() throws IOException {
        data = "With Buffer " + streamUtility.calculateTimeReadingWithBuffer("er.pdf", 1024);
        data += "\n Without Buffer " + streamUtility.calculateTimeReadingWithoutBuffer("er.pdf");
        return data;
    }

    public String showDifferenceInTimeReadingWithDifferentBufferSize(int bufferSize) throws IOException {
        data = "" + streamUtility.calculateTimeReadingWithBuffer("er.pdf", bufferSize);
        return data;
    }
}

